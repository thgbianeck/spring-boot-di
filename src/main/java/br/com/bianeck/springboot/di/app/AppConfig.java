package br.com.bianeck.springboot.di.app;

import br.com.bianeck.springboot.di.app.models.domain.InvoiceItem;
import br.com.bianeck.springboot.di.app.models.domain.Product;
import br.com.bianeck.springboot.di.app.models.services.IService;
import br.com.bianeck.springboot.di.app.models.services.MyComplexService;
import br.com.bianeck.springboot.di.app.models.services.MySimpleService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.Arrays;
import java.util.List;

@Configuration
public class AppConfig {

    @Primary
    @Bean("MySimpleService")
    public IService registerMySimpleService() {
        return new MySimpleService();
    }

    @Bean("MyComplexService")
    public IService registerMyComplexService() {
        return new MyComplexService();
    }

    @Primary
    @Bean("itemsRegister")
    public List<InvoiceItem> itemsRegister() {
        Product product1 = new Product("Sony cam", 100);
        Product product2 = new Product("Bianchi Bike aro 26", 200);
        Product product3 = new Product("Sony webcam", 400);

        return Arrays.asList(
            new InvoiceItem(product1, 2),
            new InvoiceItem(product2, 4),
            new InvoiceItem(product3, 5)
        );

    }

    @Bean("itemsRegisterOffice")
    public List<InvoiceItem> itemsRegisterOffice() {
        Product product1 = new Product("Monitor LG", 250);
        Product product2 = new Product("Notebook ASUS", 500);
        Product product3 = new Product("Impressora HP", 400);
        Product product4 = new Product("Escritorio HP", 300);

        return Arrays.asList(
                new InvoiceItem(product1, 2),
                new InvoiceItem(product2, 4),
                new InvoiceItem(product3, 5),
                new InvoiceItem(product4, 7)
        );

    }
}
