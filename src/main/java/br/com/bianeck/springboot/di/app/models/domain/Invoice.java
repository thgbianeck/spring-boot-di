package br.com.bianeck.springboot.di.app.models.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.Serializable;
import java.util.List;

@Component
@RequestScope
public class Invoice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Value("${invoice.description}")
    private String description;
    @Autowired
    private Customer customer;
    @Autowired
    private List<InvoiceItem> items;

    @PostConstruct
    public void init() {
        customer.setName(customer.getName().concat(" ").concat("de Tal"));
        description = description.concat(" of customer: ").concat(customer.getName());
    }

    @PreDestroy
    public void destroy() {
        System.out.println("invoice ".concat(description).concat(" destroyed."));
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<InvoiceItem> getItems() {
        return items;
    }

    public void setItems(List<InvoiceItem> items) {
        this.items = items;
    }
}
