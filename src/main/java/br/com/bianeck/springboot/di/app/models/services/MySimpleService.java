package br.com.bianeck.springboot.di.app.models.services;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

//@Primary
//@Service("MySimpleService")
public class MySimpleService implements IService{

    @Override
    public String operation() {
        return "executando algum processo importante simples...";
    }
}
