package br.com.bianeck.springboot.di.app.models.services;

public interface IService {

    String operation();
}
