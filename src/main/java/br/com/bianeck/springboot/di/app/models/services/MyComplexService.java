package br.com.bianeck.springboot.di.app.models.services;

import org.springframework.stereotype.Service;

//@Service("MyComplexService")
public class MyComplexService implements IService{

    @Override
    public String operation() {
        return "executando algum processo importante complexo...";
    }
}
